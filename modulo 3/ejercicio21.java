package modulo3;
import java.util.Scanner;
public class ejercicio21 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*
 De un empleado se conoce 
la categor�a: A,B,C
la antig�edad
el sueldo.
Se quiere determinar el sueldo neto sabiendo que si la antig�edad esta entre
1 y 5 a�os se le aumentan un 5% al sueldo bruto.
6 y 10 a�os se le aumenta un 10% al sueldo bruto.
Mas de 10 un 30%
 Y un plus por categor�a
A= 1000, B=2000, c=3000
 */
		Scanner c = new Scanner(System.in);
		System.out.println("Ingrese una categoria: A,B o C");
		char categoria;
		categoria = c.next().charAt(0);
		System.out.println("Ingrese los a�os de antiguedad");
		int antiguedad = c.nextInt();
		System.out.println("Ingrese su sueldo actual");
		float sueldo = c.nextFloat();
		if ((antiguedad>=1)&&(antiguedad<=5)) sueldo = (float)(sueldo*1.05);
		if ((antiguedad>=6)&&(antiguedad<=10)) sueldo = (float)(sueldo*1.10);
		if (antiguedad>10) sueldo = (float)(sueldo*1.30);
		System.out.println(sueldo);
		if (categoria=='A'){
			sueldo=sueldo+1000;
		}
		else if (categoria=='B'){
			sueldo=sueldo+2000;
		}
		else sueldo=sueldo+3000;
		System.out.println("El sueldo neto es "+sueldo);
		System.out.println("Su categoria es "+categoria);
		System.out.println("Su antiguedad es "+antiguedad+" a�os");
	}

}
